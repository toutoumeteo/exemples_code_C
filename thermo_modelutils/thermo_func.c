#include "tdpack_const.h"
#include "tdpack_func.h"
#include <math.h>
#include <stdio.h>
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

float shrahu(float hr, float tx,float px, int swtt) {
  //
  //FONCTION SHRAHU  -  PASSAGE DE HR A HU
  //
  // Version javascript par Andre Plante avril 2011
  // Version C utilisant les funtion de tcpack CMC-RPN juin 2017
  // 
  // base sur SHRAHU, de N. Brunet  (Jan91) !copyright (C) 2001  MSC-RPN COMM
  //
  //Object
  //          to return specific humidity(kg/kg) calculated from
  //          relative humidity, temperature and pressure
  //
  //Arguments
  //
  //          - Input -
  // HR       relative humidity (fraction)
  // TX       temperature or virtual temperature in Kelvins
  // PX       pressure in Pa
  // SWTT     .TRUE. to pass TT for argument
  //          .FALSE. to pass TV for argument
  float hus, tg, ee, tp, epsil = 0.001;
  int iter  = 0;
  int niter = 0;
  
  ee = min(px, hr*FOEW(tx));

  printf("ee = %f\n", ee);

  hus = FOQFE(ee,px);
  tp = FOTTV(tx,hus);
  niter = 25;
  epsil = 0.001;
  tg = 0.5*(tx+tp);
  for(iter=1;iter<=niter;iter++){
    ee = min(px,hr * FOEW(tg));
    hus = FOQFE(ee,px);
    tp = FOTTV(tx,hus);
    if(fabs(tp-tg) < epsil) {
      return hus;
    }
    tg = 0.5*(tg+tp);
  }
  return hus;
}

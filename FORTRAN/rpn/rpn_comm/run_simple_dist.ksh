npex=2
npey=2

cat > settings.nml<<EOF
&grid
  G_ni  = 3,  G_nj = 2,
  halox = 0, haloy = 0,
/
&ptopo
  npex = ${npex}, npey = ${npey}
/
EOF

((NCPUS=npex*npey))
echo "Will run with NCPUS=${NCPUS}"

#/ssm/net/rpn/MIG/ENV/d/hpcs-rpn/h201402.02-r15.2-v541-intel13sp1u2/all/bin/r.run_in_parallel -npex ${NCPUS} -npey 1 -pgm ./simple_dist

/ssm/net/rpn/MIG/ENV/d/hpcs-rpn/h201402.02-r15.2-v541-intel13sp1u2/all/bin/r.mpirun -npex ${NCPUS} -pgm ./simple_dist

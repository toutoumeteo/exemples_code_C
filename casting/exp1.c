#include <stdio.h>

int main() {

  int i1 = 100, i2 = 40;
  float f1;

  f1 = i1/i2;
  printf("No casting   : %f\n",f1);

  f1 = (float)i1/i2;  
  printf("With casting : %f\n",f1);

  return(0);

}

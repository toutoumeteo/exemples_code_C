#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char source[] = "The source string.";

int main(){

  char *dest;

  if ( (dest = strdup(source)) == NULL ) {
    fprintf(stderr, "Error allocating memory for dest.\n");
    exit(1);
  }

  printf("The destination = %s\n", dest);

  // If this free is not done, 19 bytes are lost according to valgrind.
  free(dest);
  
  return(0);

}

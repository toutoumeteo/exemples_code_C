/*
** loophello.c
*/
 
#include <mpi.h>
#include <stdio.h>
#include <unistd.h>
 
int hello(int argc, char **argv) {
    int numprocs, rank, namelen;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int loop_count, delay;
 
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(processor_name, &namelen);
 
    loop_count = 1;
    delay = 1;
    if (argc == 3) {
        sscanf(argv[1], "%d", &loop_count);
        sscanf(argv[2], "%d", &delay);
    }
    
    if(rank == 0){
      printf("ANDRE %d loop_count (%d) delay (%d)\n", rank, loop_count, delay);
    }
    for (; loop_count > 0; loop_count--) {
        printf("host (%s) rev iter (%d) process (%d) total (%d)\n", processor_name, loop_count, rank, numprocs);
        sleep(delay);
    }
 
    MPI_Finalize();
 
    fflush(NULL);
    /*sleep(5);*/
    printf("EXITING...\n");
 
    return 0;
}

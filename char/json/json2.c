#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max_array 100

int main( int argc, char *argv[]){

  int i, i0, i1, index = 0;

   
  float res[max_array];

  long ret;

  char *loc, *loc2, *sptr;
  char key[20]="data1";
  char my_buf[500] = "{\"aaaammjj\":20170522,\"hh\":6,\"ii0\":670,\"jj0\":215,\"ii1\":833,\"jj1\":444,\"pi\":456.128,\"pj\":732.405,\"d60\":10000,\"dgrw\":21,\"i\":750,\"j\":347,\"hhh\":[0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,72,78,84,90,96,102,108,114,120,126,132,138],\"data1\":[1385,1225,1096,1296,1648,1893,1521,1285,1023,842,1128,1551,1901,2088,1900,1394,1227,1063,1511,1912,2095,2066,1820,1211,1585,2212,1685,1344,1262,1243,1274,1017,1359,1989,1719]}";
  
  printf("strlen(my_buf) %lu\n", strlen(my_buf));
  
  printf("%s\n", my_buf);

  loc = strstr(my_buf, key);
  if ( !loc ){
    printf("Cannot find key %s\n", key);
  } else {
    printf("Key %s is at location %ld\n", key, loc-my_buf);
    i0 = loc-my_buf+strlen(key)+3;
    loc2 = strchr(loc+3, ']');
    if ( !loc2 ){
      printf("Cannot find terminating ] for key %s\n", key);
    } else {
      printf("] is at location %ld\n", loc2 - my_buf);
      printf("my_buf[loc2 - my_buf -1] = %c\n",my_buf[loc2 - my_buf -1]);
      i1=loc2 - my_buf -1;
    }
    i = i0;
    while( i <= i1 ){
      ret = strtol(&my_buf[i], &sptr, 10);
      if( index + 1 > max_array ){
	printf("ERREUR : max_array pas assez grande");
	return(1);
      }
      res[index] = ((float) ret) / 100.;
      printf("ret = %lu, res[index] = %f\n", ret, res[index]);
      index++;
      // move index to next int;
      i  = sptr - my_buf +1;
    }
  }
  
  return(0);
  
}

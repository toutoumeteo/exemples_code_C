#include <stdio.h>
#include <string.h>

int main( int argc, char *argv[]){

  int in_json = 0;
  int in_key =0;
  int key_b=-1;
  int key_e=-1;
  
  char my_buf[500] = "{\"aaaammjj\":20170522,\"hh\":6,\"ii0\":670,\"jj0\":215,\"ii1\":833,\"jj1\":444,\"pi\":456.128,\"pj\":732.405,\"d60\":10000,\"dgrw\":21,\"i\":750,\"j\":347,\"hhh\":[0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,72,78,84,90,96,102,108,114,120,126,132,138],\"data1\":[1385,1225,1096,1296,1648,1893,1521,1285,1023,842,1128,1551,1901,2088,1900,1394,1227,1063,1511,1912,2095,2066,1820,1211,1585,2212,1685,1344,1262,1243,1274,1017,1359,1989,1719]}";
  
  printf("strlen(my_buf) %lu\n", strlen(my_buf));
  
  printf("%s\n", my_buf);
  
  for ( int i = 0; i < strlen(my_buf); i++ ){
    if( my_buf[i] == '{' || in_json ){
      in_json = 1;
      if( my_buf[i] == '"' ){
	if( key_b < 0 ){
	  key_b = i;
	  printf("Key begins: ");
	} else {
	  key_e = i;
	}
      }
      if( key_e > 0 ){
	for ( int j = key_b+1; j < key_e; j++ ){
	  printf("%c",my_buf[j]);
	}
	printf("\n");
	key_b = -1;
	key_e = -1;
      }
    }
  }

  return(0);
  
}

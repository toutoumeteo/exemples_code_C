# -*- coding: utf-8 -*-
class Boite():
    "Object boite"
    def __init__(self, long=4, haut=3, prof=2):
        self.long = long
        self.haut = haut
        self.prof  = prof

    def volume(self):
        return "Volume de la boite %s" % (self.long * self.haut * self.prof)

    def dessus(self):
        return "Surface du dessus de la boite %d" % (self.long * self.prof)

    def devant(self):
        return "Surface du devant de la boite %d" % (self.long * self.haut)

    def cote(self):
        return "Surface du côté de la boite %d" % (self.prof * self.haut)

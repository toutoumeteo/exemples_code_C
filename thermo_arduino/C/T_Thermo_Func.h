#ifndef __T_THERMO_FUNC__
#define __T_THERMO_FUNC__

#define T_sign(x,y)  ((y >= 0) ? fabs(x) : (-fabs(x)))
#define T_min(x,y)  (((x) < (y)) ? (x) : (y))
#define T_max(x,y)  (((x) > (y)) ? (x) : (y))

// Note: 1) the use of parenthesis around ttt is necessairy for the case ttt is substituded by say a+b
//       2) the use of parenthesis around the whole formula is necessairy for the case this function is use in say a + T_foewf(ttt)
#define T_foewf(ttt) ( T_min( T_sign(T_ttns3w, (ttt)-T_trpl), T_sign(T_ttns3i, (ttt)-T_trpl) ) * fabs((ttt)-T_trpl) / ( (ttt)-T_ttns4w + T_max(0.0, T_sign(T_ttns4w-T_ttns4i,T_trpl-(ttt)) ) ) )
#define T_foew(ttt) ( T_ttns1 * exp(T_foewf(ttt)) )
#define T_foqfe(eee,prs) ( T_min(1.0,T_eps1*(eee) / ( (prs) - T_eps2*(eee) )) )
#define T_foefq(qqq,prs) ( T_min( (prs), ((qqq)*(prs)) / ( T_eps1 + T_eps2*(qqq))) )

// Inline function equivalent of T_foewf and T_foew
//static inline double T_foewf(float ttt){
//  return T_min( T_sign(T_ttns3w, ttt-T_trpl), T_sign(T_ttns3i, ttt-T_trpl) )
//    *fabs(ttt-T_trpl)
//    /( ttt-T_ttns4w + T_max(0.0, T_sign(T_ttns4w-T_ttns4i,T_trpl-ttt)) );
//}

//static inline double T_foew(float ttt){
//  return T_ttns1 * exp(T_foewf(ttt));
//}

float T_shrahu(float hr, float tt, float ps, int swph){
  float ee;
  
  if(swph){    
    ee = T_min(ps, hr * T_foew(tt) );
  } else {
    printf("To do in T_shrahu\n");
    return -1;
    //ee = T_min(ps, hr * foewa(tt) );
  }
  return T_foqfe(ee, ps);
}

float T_shuaes(float hu, float tt, float ps, int swph){
  float petit = 0.000001;
  float ee, cte, td;
  ee = T_foefq(T_max(petit,hu),ps);
  cte = log(ee/T_ttns1);
  td = (T_ttns4w*cte - T_ttns3w*T_trpl) / (cte - T_ttns3w);
  if( td < T_trpl && swph ){
    td = ( T_ttns4i*cte - T_ttns3i*T_trpl)/(cte - T_ttns3i);
  }
  return tt-td;
}

float T_shraes(float hr, float tt, float ps, int swph){

  float hus;
  hus = T_shrahu(hr,tt,ps,swph);
  return T_shuaes(hus,tt,ps,swph);

}


#endif

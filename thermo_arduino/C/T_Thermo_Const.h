#ifndef __T_THERMO_CONST__
#define __T_THERMO_CONST__

#define T_water_switch 1
#define T_ice_switch 0
#define T_ttns3w 17.269
#define T_trpl 0.2731600000000e+03
#define T_ttns3i 21.875
#define T_ttns4w 35.86
#define T_ttns4i 7.66
#define T_tcdk 0.2731500000000e+03
#define T_ttns1 610.78
#define T_eps1 0.6219800221014
#define T_eps2 0.3780199778986
#endif

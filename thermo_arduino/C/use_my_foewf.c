#include <stdio.h>
#include <math.h>

#include "T_Thermo.h"

int main( int argc, char *argv[]){

  FILE *fp;
  int i, j, ok;
  float tt;
  double es;

  fp=fopen("../data/foewf.txt", "r");
  
  ok = 1;
  for( i=-20; i<=40; i++){
    for( j=0; j<=9; j++){
      fscanf(fp, "%f %lf", &tt, &es);
      if( fabs(es - T_foewf(tt+T_tcdk)) > .00001){
	ok = 0;
	printf("%f %lf %lf\n", tt, es, T_foewf(tt+T_tcdk));
      }
    }
  }  
  fclose(fp);
  if(ok){
    printf("Test passed\n");
    return 0;
  }else{
    printf("Test failed\n");
    return 1;
  }
} 

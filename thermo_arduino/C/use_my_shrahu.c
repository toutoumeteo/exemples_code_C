#include <stdio.h>
#include <math.h>

#include "T_Thermo.h"

int main( int argc, char *argv[]){

  FILE *fp;
  int i, j, k, ok;
  float tt, hr, ps, hu, res;

  fp=fopen("../data/shrahu.txt", "r");
  
  ok = 1;
  for( i=-20; i<=40; i++){
    for( j=1; j<=10; j++){      
      for( k=990; k<=1020; k+=10){
	fscanf(fp, "%f %f %f %f", &hr, &tt, &ps, &res);
	if( fabs( res-T_shrahu(hr,tt+T_tcdk,ps*100.,1) ) / res > .00001){
	  ok = 0;
	  printf("%f %f %f %f %f\n", hr, tt, ps, res, T_shrahu(hr,tt+T_tcdk,ps*100.,1));
	}
      }
    }
  }  
  fclose(fp);
  if(ok){
    printf("Test passed\n");
    return 0;
  }else{
    printf("Test failed\n");
    return 1;
  }

} 

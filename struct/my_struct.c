#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define VGD_MAXSTR_NOMVAR 5
#define VGD_MAXSTR_TYPVAR 3
#define VGD_MAXSTR_ETIKET 13
#define VGD_MAXSTR_GRTYP  2
#define VGD_MISSING  -9999.

#define FREE(x) if(x) { free(x); x=NULL; }

typedef struct VGD_TFSTD {
   int   dateo;                 // date d'origine du champs
   int   deet;                  // duree d'un pas de temps
   int   npas;                  // pas de temps
   int   nbits;                 // nombre de bits du champs
   int   datyp;                 // type de donnees
   int   ip1,ip2,ip3;           // specificateur du champs
   int   ig1,ig2,ig3,ig4;       // descripteur de grille
   char  typvar[VGD_MAXSTR_TYPVAR]; // type de variable
   char  nomvar[VGD_MAXSTR_NOMVAR]; // nom de la variable
   char  etiket[VGD_MAXSTR_ETIKET]; // etiquette du champs
   char  grtyp[VGD_MAXSTR_GRTYP];   // type de grilles
   char  fstd_initialized;      // if the fstd struct is initialized
} VGD_TFSTD;

typedef struct vgrid_descriptor {
  VGD_TFSTD rec;          // RPN standard file header
  double   ptop_8;        // Top level pressure (Pa)
  double   pref_8;        // Reference pressure (Pa)
  double   *table;        // Complete grid descriptor record
  int      table_ni;      //    ni size of table
  int      table_nj;      //    nj size of table
  int      table_nk;      //    nk size of table
  double   *a_m_8;        // A-coefficients for momentum levels  
  double   *b_m_8;        // B-coefficients for momentum levels
  double   *a_t_8;        // A-coefficients for thermodynamic levels
  double   *b_t_8;        // B-coefficients for thermodynamic levels
  int      *ip1_m;        // ip1 values for momentum levels
  int      *ip1_t;        // ip1 values for momentum levels
  int      nl_m;          // Number of momentum      level (size of a_m_8, b_m_8 and ip1_m)
  int      nl_t;          // Number ot thermodynamic level (size of a_t_8, b_t_8 and ip1_t)
  float    dhm;           // Diag level Height (m) for Momentum variables UU,VV
  float    dht;           // Diag level Height (t) for Thermo variables TT,HU, etc
  char*    ref_name;      // Reference field name
  float    rcoef1;        // Rectification coefficient
  float    rcoef2;        // Rectification coefficient
  int      nk;            // Number of momentum levels
  int      ip1;           // ip1 value given to the 3D descriptor
  int      ip2;           // ip2 value given to the 3D descriptor
  int      unit;          // file unit associated with this 3D descriptor
  int      vcode;         // Vertical coordinate code
  int      kind;          // Vertical coordinate code
  int      version;       // Vertical coordinate code
  char     match_ipig;    // do ip/ig matching for records
  char     valid;         // Validity of structure
} vgrid_descriptor;

static vgrid_descriptor* c_vgd_construct() {
   vgrid_descriptor *vgrid = malloc(sizeof(vgrid_descriptor));

   if( !vgrid ){
     printf("(Cvgd) ERROR in c_vgd_construct, cannot allocate vgrid\n");
     return NULL;
   }

   if( vgrid ) {
      vgrid->ptop_8        = VGD_MISSING;
      vgrid->pref_8        = VGD_MISSING;
      vgrid->table         = NULL;
      vgrid->table_ni      = 0;
      vgrid->table_nj      = 0;
      vgrid->table_nk      = 0;
      vgrid->a_m_8         = NULL;
      vgrid->b_m_8         = NULL;
      vgrid->a_t_8         = NULL;
      vgrid->b_t_8         = NULL;
      vgrid->ip1_m         = NULL;
      vgrid->ip1_t         = NULL;
      vgrid->nl_m          = 0;
      vgrid->nl_t          = 0;
      vgrid->dhm           = VGD_MISSING;
      vgrid->dht           = VGD_MISSING;
      vgrid->ref_name      = strdup("None");
      vgrid->rcoef1        = VGD_MISSING;
      vgrid->rcoef2        = VGD_MISSING;
      vgrid->nk            = 0;
      vgrid->ip1           = 0;
      vgrid->ip2           = 0;
      vgrid->unit          = 0;
      vgrid->vcode         = 0;
      vgrid->kind          = 0;
      vgrid->version       = 0;
      vgrid->match_ipig    = 0;
      vgrid->valid         = 0;

      vgrid->rec.fstd_initialized = 0;
      vgrid->rec.dateo = 0;
      vgrid->rec.deet = 0;
      vgrid->rec.npas = 0;
      vgrid->rec.nbits = -64;
      vgrid->rec.datyp = 0;
      vgrid->rec.ip1 = 0;
      vgrid->rec.ip2 = 0;
      vgrid->rec.ip3 = 0;
      vgrid->rec.ig1 = 0;
      vgrid->rec.ig2 = 0;
      vgrid->rec.ig3 = 0;
      vgrid->rec.ig4 = 0;
      strcpy(vgrid->rec.typvar,"  ");
      strcpy(vgrid->rec.nomvar,"    ");
      strcpy(vgrid->rec.etiket,"            ");
      strcpy(vgrid->rec.grtyp," ");
   }

   return(vgrid);
}
static void c_vgd_free_abi(vgrid_descriptor **self) {
  if( *self ) {
    // Thermo pointers may be pointing to momentum for certain Vcode, only nullify them if this is the case.
    if( (*self)->a_t_8 == (*self)->a_m_8 ) {
      (*self)->a_t_8 = NULL;
    } else {
      FREE((*self)->a_t_8);      
    }
    if( (*self)->b_t_8 == (*self)->b_m_8 ) {
      (*self)->b_t_8 = NULL;
    } else {
      FREE((*self)->b_t_8);
    }
    if( (*self)->ip1_t == (*self)->ip1_m ) {
      (*self)->ip1_t = NULL;
    } else {
      FREE((*self)->ip1_t);
    }
    FREE((*self)->a_m_8);
    FREE((*self)->b_m_8);
    FREE((*self)->ip1_m);
  }
}

void Cvgd_free(vgrid_descriptor **self) {
   if( *self ) {
      FREE((*self)->table);
      c_vgd_free_abi(self);
      FREE((*self)->ref_name);      
      free(*self);
      *self = NULL;
   }
}

void main () {

  vgrid_descriptor *vgrid;
  
  vgrid = c_vgd_construct();

  Cvgd_free(&vgrid);

}

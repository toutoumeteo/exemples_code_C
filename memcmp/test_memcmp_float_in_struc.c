/* Example using memcmp by TechOnTheNet.com */

#include <stdio.h>
#include <string.h>

typedef struct my_struc {
  float ff;
} my_struc;

int main(int argc, const char * argv[])
{
  int result, i, n=50;

  my_struc example1, *p1;
  my_struc example2, *p2;

  example1.ff=1.;
  example2.ff=2.;
  p1=&example1;
  p2=&example2;

  //result = memcmp(&(example1.ff), &(example2.ff), sizeof(float)/sizeof(char));
  result = memcmp(&(p1->ff), &(p2->ff), sizeof(float)/sizeof(char));

  if (result == 0){
    printf("Structure members ff are the same\n");
  } else {
    printf("Structure members ff are different\n");       
  }

  return 0;

}


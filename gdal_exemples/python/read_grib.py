#http://www.gdal.org/gdal_tutorial.html

#import os
#from osgeo import gdal, ogr, osr

import gdal
from gdalconst import *
import struct

#================
#Opening the File
#----------------
src_filename="CMC_reg_PRMSL_MSL_0_ps10km_2016121512_P024.grib2"
dataset = gdal.Open(src_filename, GA_ReadOnly)

if dataset is None:
    print 'Unable to open %s' % src_filename
    sys.exit(1)

#===========================
#Getting Dataset Information
#---------------------------
print 'Driver: ', dataset.GetDriver().ShortName,'/', \
      dataset.GetDriver().LongName
print 'Size is ',dataset.RasterXSize,'x',dataset.RasterYSize, \
      'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'

#======================
#Fetching a Raster Band
#----------------------
band = dataset.GetRasterBand(1)
print 'Band Type=',gdal.GetDataTypeName(band.DataType)
min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)
if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'
if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

#===================
#Reading Raster Data
#-------------------
scanline = band.ReadRaster( 0, 0, band.XSize, 1, \
                             band.XSize, 1, GDT_Float32 )
tuple_of_floats = struct.unpack('f' * band.XSize, scanline)

print tuple_of_floats[:]

#==============
# close dataset
#--------------
dataset = None
